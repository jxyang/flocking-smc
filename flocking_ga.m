function [x, fval] = flocking_ga()
nvars = 4;

%constrain: Aeq * x = Beq
%weights sum up to 1
Aeq = ones(1,nvars);
Beq = 1;

%lower bound & upper bound
LB = zeros(nvars,1);
UB = ones(nvars,1);

gaoptions = gaoptimset('UseParallel','always');
%genetic algo
% m is the number of runs
m = 5;
x = zeros(m, nvars);
fval = zeros(m, 1);
tic;
for i=1:m
	[x(i,:), fval(i)] = ga(@flocking_obj, nvars, [], [], Aeq, Beq, LB, UB, [], gaoptions);
end
toc;
end

function fitness = flocking_obj(w)
%[b, fitness] = flock_sim(w);
[mu1, fitness, N, z] = SRA(0.5,0.5,w);
end