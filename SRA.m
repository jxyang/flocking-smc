function [ mu1, mu2, N, Z ] = SRA( e, d, w )

% r = 4*(exp(1)-2)*log(2/d)/(e*e);
% r1 = 1+(1+e)*r;
% disp(r1);
% Sb = 0;
% Sr = 0;
% N = 0;
% tic;
% while Sb <= r1 || Sr <= r1
%     N = N + 1;
%     [b,r] = flock_sim();
%     Z(N) = r;
%     Sb = Sb + b;
%     Sr = Sr + r;
% end
% mu1 = Sb/N;
% mu2 = Sr/N;
% toc;
Sb = 0;
Sr = 0;
N = 4*log(2/d)/(e*e);
for i=1:N
    [b,r] = flock_sim(w);
    Z = r;
    Sb = Sb + b;
    Sr = Sr + r;
end
mu1 = Sb/N;
mu2 = Sr/N;
end

