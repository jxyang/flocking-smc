function [ b, r ] = Ftester( x, y )
%FTESTER Summary of this function goes here
%   Detailed explanation goes here
global n;
n = size(x,1);

p_msg = zeros(n,2)-1;
f_msg = zeros(n,2)-1;

for i=1:n
    p_msg(i,:) = p_tran(x, y, i);
end
for i=n:-1:1
    f_msg(i,:) = f_tran(p_msg, f_msg, i);
end

b = f_msg(1,1);
if b == false
    r = 0;
else
    r = f_msg(1,2);
end
end

function out = p_tran(x, y, k)
out = zeros(2,1);
out(1) = false;
d0 = 0.3;
penalty = 0;
num = size(x,2);
for i=1:num
    for j=1:num
        if j~= i
            d=norm([x(k,i)-x(k,j), y(k,i)-y(k,j)]);
            if d <= d0
                out(1) = true;
                penalty = penalty + d0 - d;
            end
        end
    end
end
out(2) = penalty/(num*(num-1)*d0);
end

function out = f_tran(p_msg, f_msg, i)
global n;
out = zeros(2,1);
if i==n
    rb = false;
else
    rb = f_msg(i+1,1);
end
ib = p_msg(i,1);
if ib==-1 || rb==-1
    out(1)=-1;
else
    out(1)= ib || rb;
end
if i==n
    rp = 0; 
else
    rp = f_msg(i+1,2);
end
ip = p_msg(i, 2);
if out(1)~=-1
    if ib == true
        out(2) = ip;
    else
        out(2) = rp;
    end
else
    out(2) = -1;
end
end