function [ b,r ] = flock_collision( )
w = [0.6 0.1 0.2 0.1];
% num of agents
num = 10;
% simulation time
steps = 50;
% initial position range
init_box = 10;
% initial velocity range
init_v = 1;

% radius for centering
centering_r = 5;
% radius for repulsion
rep_r = 3;
% minimun distance between agents
d0 = 0.1;
% x,y : position; vx,vy : velocity
x = zeros(steps, num);
y = zeros(steps, num);
vx = zeros(steps, num);
vy = zeros(steps, num);

% initial values
x(1,:)= init_box*(rand(1,num));
y(1,:)= init_box*(rand(1,num));
vx(1,:)= init_v*(rand(1,num));
vy(1,:)= init_v*(rand(1,num));

maxv = 2;

for t=1:steps
	% velocity matching
	vm = v_matching(vx,vy,t,num);
	for i=1:num
		%new position
		x(t+1,i)=x(t,i)+vx(t,i);
		y(t+1,i)=y(t,i)+vy(t,i);
		
		avg_v = [0 0]; ca = [0 0];
		center = [0 0]; neigh1 = 0;
		offset = [0 0]; neigh2 = 0;
		
		for j=1:num
			if j ~= i
				d=norm([x(t,i)-x(t,j), y(t,i)-y(t,j)]);
				
				% velocity averaging(C-D model)
				% this weight a comes from cucker's model
				a=0.1/((1+d^2)^(1/3));
				avg_v = avg_v + a*[vx(t,j)-vx(t,i) vy(t,j)-vy(t,i)];
				
				% collision avoidance(C-D)
				f = 1/((d^2 - d0)^2);
				ca = ca + f*[x(t,i)-x(t,j) y(t,i)-y(t,j)];
				
				%centering(Reynolds)
				if d <= centering_r
					center = center + [x(t,j) y(t,j)];
					neigh1 = neigh1 + 1;
				end
				
				%repulsion(Reynolds)
				if d <= rep_r
					offset = offset + [x(t,i)-x(t,j) y(t,i)-y(t,j)];
					neigh2 = neigh2 + 1;
				end
				
			end
		end
		
		ca = vm*ca;
		
		% compute acceleration for centering and repulsion
		if neigh1 > 0
			center = center/neigh1;
			A1 = atan2(center(2)-y(t,i), center(1)-x(t,i));
			center_acc = steering(vx, vy, t, i, A1);
		else
			center_acc = [0 0];
		end
		
		if neigh2 > 0
			A2 = atan2(offset(2), offset(1));
			rep_acc = steering(vx,vy,t,i,A2);
		else
			rep_acc = [0 0];
		end
		
		% new velocity
		vx(t+1, i) = vx(t,i) + w(1)*avg_v(1) + w(2)*ca(1) + w(3)*center_acc(1) + w(4)*rep_acc(1);
		vy(t+1, i) = vy(t,i) + w(1)*avg_v(2) + w(2)*ca(2) + w(3)*center_acc(2) + w(4)*rep_acc(2);
		
		[vx(t+1,i),vy(t+1,i)] = trim_v(vx,vy,t+1,i,maxv);
	end
	
end

[b,r] = Ftester(x,y);

end

% velocity matching
% vm = (1/k \sum_{j>i} ||v_j - v_i||^2)^(1/2)
% vm = 0 when all velocity are the same
% known as alignment measure in C-D model
function vm = v_matching(vx,vy,t,num)

sum = 0;
for i=1:num
	for j=i+1:num
		diff = norm([vx(t,i)-vx(t,j) vy(t,i)-vy(t,j)]);
		sum = sum + diff^2;
	end
end
vm = (sum/num)^(1/2);
end

% steering computes the steering force(vector):
% Steering = (Desired - current velocity)*smooth coefficient
function [xy] = steering(vx, vy, t, i, A)
v_len = norm([vx(t,i) vy(t,i)]);
desired = [v_len*cos(A) v_len*sin(A)];
xy = desired - [vx(t,i) vy(t,i)];
end

function [newX,newY] = trim_v(vx, vy, t, i, maxv)
v_len = norm([vx(t,i) vy(t,i)]);
if v_len <= maxv
	newX = vx(t,i);
	newY = vy(t,i);
else
	theta = atan2(vy(t,i),vx(t,i));
	newX = maxv*cos(theta);
	newY = maxv*sin(theta);
end
end

