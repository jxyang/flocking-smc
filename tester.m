function [ b, r ] = tester( traj )
global n;
n = size(traj,1);

p_msg = zeros(n,2)-1;
g_msg = zeros(n,3)-1;
fg_msg = zeros(n,4)-1;
%for m=1:n
for i=1:n
    p_msg(i,:) = p_tran(traj, i);
end
for i=n:-1:1
    g_msg(i,:) = g_tran(p_msg, g_msg, i);
end
for i=n:-1:1
    fg_msg(i,:) = fg_tran(g_msg, fg_msg, i);
end
%end
b = fg_msg(1,1);
if b == false
    r = 0;
else
    r = fg_msg(1,4);
end
end

function out = p_tran(traj, i)
out = zeros(2,1);
out(1) = traj(i) <= 0.05;
out(2) = traj(i);
end

function out = g_tran(p_msg, g_msg, i)
global n;
out = zeros(3,1);
if i==n
    rb = true;
else
    rb = g_msg(i+1,1);
end
ib = p_msg(i,1);
if ib==-1 || rb==-1
    out(1)=-1;
else
    out(1)= ib && rb;
end
ivm = p_msg(i,2);
if i==n
    ravm = 0; rk = 0;
else
    ravm = g_msg(i+1,2); rk = g_msg(i+1,3);
end
if out(1)~=-1
    out(2) = (ivm + ravm*rk)/(rk+1);
    out(3) = rk+1;
else
    out(2) = -1;
    out(3) = -1;
end
end

function out = fg_tran(g_msg, fg_msg, i)
global n;
out = zeros(4,1);
if i==n
    rb = false;
else
    rb = fg_msg(i+1,1);
end
ib = g_msg(i,1);
if ib==-1 || rb==-1
    out(1)=-1;
else
    out(1)= ib || rb;
end
iavm = g_msg(i,2);
if i==n
    ravm = 0; rk = 0;
else
    ravm = fg_msg(i+1,2); rk = fg_msg(i+1,3);
end
if out(1)~=-1
    if ib == true
        out(2) = iavm;
        out(3) = 1;
    else
        out(2) = ravm;
        out(3) = rk+1;
    end
else
    out(2) = -1;
    out(3) = -1;
end
out(4) = 0.01*(out(3)/50) + 0.99*out(2);
end