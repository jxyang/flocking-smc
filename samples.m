function [ b, u, N ] = samples( )
%SAMPLES Summary of this function goes here
%   Detailed explanation goes here
m = 5;
n = 1;
b = zeros(m+2,n);
u = zeros(m+2,n);
N = zeros(m+2,n);
k = 1;
for e=[ 0.3]
    for d=[0.3 ]
        for i=1:m
            [b(i,k), u(i,k), N(i,k), z] = SRA(e, d);
        end
        k = k + 1;
    end
end
for k=1:n
    b(m+1,k) = mean(b(1:m,k));
    b(m+2,k) = std(b(1:m,k));
    u(m+1,k) = mean(u(1:m,k));
    u(m+2,k) = std(u(1:m,k));
    N(m+1,k) = mean(N(1:m,k));
    N(m+2,k) = std(N(1:m,k));
end
end