function [ mu1, mu, N ] = OAA( e, d )

r = 4*(exp(1)-2)*log(2/d)/(e*e);
r2 = 2*(1+sqrt(e))*(1+2*sqrt(e))*(1+log(3/2)/log(2/d))*r;

w = [0.6 0.1 0.2 0.1];

[u0, u, n, Z] = SRA(sqrt(e), d/3, w); disp(n);
N1 = r2*e/u; disp(N1);
S = 0;
for i=1:N1
    [b1,r0] = flock_sim(w);
    [b2,r1] = flock_sim(w);
    S = S + ((r0 - r1)^2)/2;
end
p = max(S/N1, e*u);
N2 = r2*p/(u*u); disp(N2);
S = 0;
for i=1:N2
    if i > n
        [b Z(i)] = flock_sim(w);
    end
    S = S + Z(i);
end
mu1 = 1;
mu = S/N2;
if n > N2
    N = n + 2*N1;
else
    N = N2+ 2*N1;
end
end

