function [ b, r ] = testers( traj )
n = size(traj,1);

%p tester state
p_state.step = n;

%g tester state
g_state.gb = true;
g_state.avm = 0;
g_state.k = 0;
g_state.step = n;

%fg tester state
fg_state.fgb = false;
fg_state.k = inf;
fg_state.avm = 0;
fg_state.step = n;
fg_state.lc = 0;

%messages
p_msg = zeros(n,2)-1;
g_msg = zeros(n,2)-1;

while true
    if p_state.step ~= 0
        [p_msg, p_state] = p_tran(p_state, traj);
    end
    if g_state.step ~= 0
        [g_msg, g_state] = g_tran(g_state, p_msg);
    end
    if fg_state.step ~= 0
        fg_state = fg_tran(fg_state, g_msg);
    end
    if p_state.step==0 && g_state.step==0 && fg_state.step==0
        break;
    end
end
b = fg_state.fgb;
r = fg_state.lc;
end

function [out, newState] = p_tran(state, in)
out(state.step,:) = [in(state.step) <= 0.05, in(state.step)];
newState.step = state.step - 1;
end

function [out, newState] = g_tran(state, in)
newState.gb = state.gb & in(state.step,1);
newState.avm = (state.avm * state.k + in(state.step,2))/(state.k+1);
newState.k = state.k + 1;
out(state.step,:) = [newState.gb newState.avm];
newState.step = state.step - 1;
end

function [newState] = fg_tran(state, in)
newState.fgb = state.fgb | in(state.step,1);
if in(state.step,1)
    newState.k = 1;
    newState.avm = in(state.step,2);
else
    newState.k = state.k + 1;
    newState.avm = state.avm;
end
newState.step = state.step - 1;
if newState.step == 0
    newState.lc = 0.01*(newState.k/50) + 0.99*newState.avm;
end
end
